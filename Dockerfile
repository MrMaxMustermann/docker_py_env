FROM python:3.7

WORKDIR /usr/src/app
COPY ./Requirements.txt .
RUN pip install -r Requirements.txt

CMD for f in *.py; do python3 "$f"; done


