#!/bin/bash

docker image build -t my_python .
docker container run --rm -v $PWD/app/:/usr/src/app/ my_python
