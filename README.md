# A Docker based python environment

## TL;DR
Put python files into the ./app folder and run the 'runfile'. The necessary packages are in the Requirements.txt; numpy, matplotlib and scipy are the standard. Results from the scripts can be found in the ./app folder as well
