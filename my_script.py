import numpy as np
from matplotlib import pyplot as plt

x=np.arange(1000)
y=np.sin(x)

plt.figure()
plt.plot(x,y)

plt.show()
plt.savefig('my_script_output.png')
